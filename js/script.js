let userName
let userAge

do{
  userName = prompt('Enter your name:', userName)
}
while (userName === null || userName.trim() === '' || userName.length <2)

do{
  userAge = prompt('Enter your age:', userAge)
}
while (userAge === null || userAge < 0 || Number.isNaN(+userAge) || userAge.trim() === '')

userAge = +userAge

if (userAge < 18) { 
  alert('You are not allowed to visit this website')
}
else if (userAge <= 22) {
  if (confirm ('Are you sure you want to continue?'))
    alert(`Welcome, ${userName}`)  
  else
    alert('You are not allowed to visit this website.')
}
else
  alert(`Welcome, ${userName}`)
